# think - focus for good
-----

More details and agenda for development will be added later. 

To Run: 
    1. make sure [cocoapods](https://cocoapods.org) is installed.
    2. open the workspace file, not the project file.
    3. change the project signing accordingly.
    4. build and run.
